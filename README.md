# apkgquery

A command-line tool to query the metadata of aports or other repositories with
APKBUILDs files.

## Installation

``` sh
go install gitlab.alpinelinux.org/kdaudt/apkgquery@latest
```

## Usage

``` sh
apkgquery [-r|--root <path>] <query>
```

| Option        | Description                      |
|---------------|----------------------------------|
| `-r / --root` | Path to directory with packages. |


## Query language

This tool uses [expr][] as a query language. A `pkg` object with the type
[Apkbuild][go-apkbuild] is exposed with the metadata of a package. When the
query expression evaluates to true, the package name is printed.

In addition to the functions provided by `expr`, this tool also provides some
convience functions:

| Function           | Description                                                                                               |
|--------------------|-----------------------------------------------------------------------------------------------------------|
| `print(msg)`       | Always evaluates to true. Can be chained with `&&` to print aditional information for each found package. |

[expr]: https://github.com/antonmedv/expr/blob/master/docs/Language-Definition.md
[go-apkbuild]: https://pkg.go.dev/gitlab.alpinelinux.org/alpine/go/apkbuild#Apkbuild
[go-strings-split]: https://pkg.go.dev/strings#Split
[go-strings-join]: https://pkg.go.dev/strings#Join

## Examples

### Pkgrel is larger then 10

``` sh
apkgquery -r ~/aports/ 'int(pkg.Pkgrel) > 10'
```

### Includes MIT as license

``` sh
apkgquery -r ~/aports/ '"MIT" in split(pkg.License, " ")'
```

### Provides version is incorrect and therefore considered part of the package name

``` sh
apkgquery -r ~/aports/ 'any(pkg.Provides, {.Constraint == ""}) && any(pkg.Provides, {.Pkgname matches "\\d+.\\d+-r?\\d$" && print(.Pkgname)})'
```

### Provides virtual package without setting a provider_priority

``` sh
apkgquery -r ~/aports/ 'any(pkg.Provides, {.Constraint == ""}) && pkg.ProviderPriority == "" && print(pkg.Provides[0].Pkgname)'
```

