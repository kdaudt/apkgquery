module gitlab.alpinelinux.org/kdaudt/apkgquery

go 1.21

require (
	github.com/expr-lang/expr v1.16.6
	github.com/spf13/pflag v1.0.5
	gitlab.alpinelinux.org/alpine/go v0.10.0
	mvdan.cc/sh/v3 v3.8.0
)

require (
	golang.org/x/sync v0.6.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/term v0.17.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
