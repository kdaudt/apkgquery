package main

import (
	"bytes"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/expr-lang/expr"
	"github.com/expr-lang/expr/vm"
	"github.com/spf13/pflag"
	"gitlab.alpinelinux.org/alpine/go/apkbuild"
	// "gitlab.alpinelinux.org/alpine/go/version"
	"mvdan.cc/sh/v3/expand"
)

var (
	optNoPkgName   bool
	optNoPredicate bool
	optDebug       bool
)

func exitError(err error) {
	fmt.Fprintf(os.Stderr, "error: %s\n", err)
	os.Exit(1)
}

func debug(msg string) {
	if optDebug {
		fmt.Printf("%s\n", msg)
	}
}

func main() {
	root := pflag.StringP("root", "r", ".", "The root dir to look for packages, defaults to PWD")
	pflag.BoolVarP(&optNoPkgName, "no-package-name", "n", false, "Do not print the package name")
	pflag.BoolVarP(&optNoPredicate, "no-predicate", "P", false, "Do not print the package name (implies -n)")
	pflag.BoolVarP(&optDebug, "debug", "d", false, "Enable debug output")
	pflag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] <expr>\n", os.Args[0])
		pflag.PrintDefaults()
	}
	pflag.Parse()

	expression := pflag.Arg(0)

	if expression == "" {
		pflag.Usage()
		return
	}

	program, err := expr.Compile(expression)
	if err != nil {
		exitError(err)
	}

	shellEnv := expand.ListEnviron(os.Environ()...)
	discoveredPackages := make(chan apkbuild.ApkbuildFile, 4)

	wg := sync.WaitGroup{}
	wg.Add(1)
	go EvaluatePackages(discoveredPackages, program, shellEnv, &wg)

	err = filepath.WalkDir(*root, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		var apkbuildPath string
		switch {
		case d.Type().IsDir():
			apkbuildPath = filepath.Join(path, "APKBUILD")
		case d.Name() == "APKBUILD":
			apkbuildPath = d.Name()
		default:
			return nil
		}

		f, err := os.Open(apkbuildPath)
		if err != nil {
			if errors.Is(err, fs.ErrNotExist) {
				return nil
			}
			return err
		}

		parents, pkg := filepath.Split(path)
		repo := filepath.Base(parents)
		discoveredPackages <- apkbuild.NewApkbuildFile(filepath.Join(repo, pkg), f)

		return nil
	})

	close(discoveredPackages)
	wg.Wait()

	if err != nil {
		exitError(err)
	}
}

func EvaluatePackages(apkbuildFiles chan apkbuild.ApkbuildFile, program *vm.Program, shellEnv expand.Environ, wg *sync.WaitGroup) {
	for apkbuildFile := range apkbuildFiles {
		debug(fmt.Sprintf("Found package %s", apkbuildFile.PackageName))
		apkbld, err := apkbuild.Parse(apkbuild.NewApkbuildFile(apkbuildFile.PackageName, apkbuildFile.Content), shellEnv)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %s: %s", apkbuildFile.PackageName, err)
			continue
		}

		outbuffer := strings.Builder{}

		exprEnv := map[string]interface{}{
			"pkg": apkbld,
			"printf": func(format string, args ...interface{}) bool {
				fmt.Fprintf(&outbuffer, format, args...)
				return true
			},
			"print": func(msg interface{}) bool {
				fmt.Fprint(&outbuffer, msg)
				return true
			},
			// "isValidVersion": func(v string) bool {
			// 	return version.IsValid(v)
			// },
		}

		output, err := expr.Run(program, exprEnv)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %s: %s", apkbuildFile.PackageName, err)
			continue
		}

		printBuffer := &bytes.Buffer{}
		fmt.Fprint(printBuffer, outbuffer.String())

		if optNoPredicate {
			fmt.Printf("%s", printBuffer)
			if val, ok := output.(string); ok {
				fmt.Printf("%s\n", val)
			}
		} else {
			if predicate, ok := output.(bool); ok {
				if !optNoPkgName {
					fmt.Fprint(printBuffer, apkbuildFile.PackageName)
				}
				if outbuffer.Len() > 0 {
					if !optNoPkgName {
						fmt.Fprint(printBuffer, ": ")
					}
				}
				if predicate {
					fmt.Printf("%s\n", printBuffer)
				}
			}
		}
	}
	wg.Done()
}
